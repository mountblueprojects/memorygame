const gameContainer = document.getElementById("game");
let winValue = 8;
let isFlipped = 0;
let count = 0;
let openCards = 0;
let previousId;
let previousClass;

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want to research more
function shuffle() {
  let COLORS = [
    "red 1",
    "red 1",
    "blue 2",
    "blue 2",
    "green 3",
    "green 3",
    "orange 4",
    "orange 4",
    "purple 5",
    "purple 5",
    "yellow 6",
    "yellow 6",
    "brown 7",
    "brown 7",
    "blueviolet 8",
    "blueviolet 8",
    "maroon 9",
    "maroon 9",
    "orangered 10",
    "orangered 10",
    "violet 11",
    "violet 11",
    "black 12",
    "black 12",
  ];
  if (document.getElementById("select-difficulty").value === "easy") {
    COLORS = COLORS.slice(0, 8);
  } else if (document.getElementById("select-difficulty").value === "medium") {
    COLORS = COLORS.slice(0, 16);
  } else {
    COLORS = COLORS.slice(0, 24);
  }

  let counter = COLORS.length;

  // While there are elements in the COLORS
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = COLORS[counter];
    COLORS[counter] = COLORS[index];
    COLORS[index] = temp;
  }

  return COLORS;
}

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card

function createDivsForColors(colorArray) {
  let index = 0,
    card = 0;

  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color.split(" ")[0]);
    newDiv.classList.add("card");

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    newDiv.id = `${index}`;

    newDiv.innerHTML = `<img class="front-face" src="./gifs/${
      color.split(" ")[1]
    }.gif">
    <img class="back-face" src="./gifs/cute-cool-sun-drink-coffee-cartoon-vector-icon-illustration-nature-drink-icon-concept-isolated-flat_138676-4592.webp">`;
    // append the div to the element with an id of game
    gameContainer.append(newDiv);

    index++;
    card++;
    if (card === 6) {
      card = 0;
    }
  }

  //remove previous card's class and id when new game is started
  previousId = null;
  previousClass = null;
}

//set the number of cards in the game depending on difficulty
function setCards() {
  if (document.getElementById("select-difficulty").value === "medium") {
    winValue = 16;
  } else if (document.getElementById("select-difficulty").value === "hard") {
    winValue = 24;
  } else {
    winValue = 8;
  }
}

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked

  //prevent click when div is clicked
  if (event.target.classList[1] === "card") {
    return;
  }

  //prevent card click when two cards are open
  if (isFlipped === 1) {
    return;
  }

  // prevent clicking on same card twice
  if (previousId === event.target.parentElement.id) {
    return;
  }

  // increment openCards and count
  openCards++;
  count++;

  //set score in live-score div in html
  document.getElementById("live-score").innerText = `${count}`;

  //set background color for the card and flip it when clicked
  event.target.parentElement.style.background =
    event.target.parentElement.classList[0];
  document.getElementById(event.target.parentElement.id).classList.add("flip");

  //set previous class and id if openCards is odd
  if (openCards % 2 === 1) {
    previousId = event.target.parentElement.id;
    previousClass = event.target.parentElement.classList[0];
  } else if (event.target.parentElement.classList[0] !== previousClass) {
    //flip the cards back when they are not matched and set isFlipped flag to prevent clicking other cards
    isFlipped = 1;

    setTimeout(() => {
      document.getElementById(previousId).classList.remove("flip");
      event.target.parentElement.classList.remove("flip");

      document.getElementById(previousId).style.background = "none";
      event.target.parentElement.style.background = "none";

      openCards -= 2;
      isFlipped = 0;
    }, 1000);
  } else {
    //remove click event for matched cards
    event.target.parentElement.removeEventListener("click", handleCardClick);
    document
      .getElementById(previousId)
      .removeEventListener("click", handleCardClick);

    if (openCards === winValue) {
      //display score and set high score when the game is over
      setTimeout(() => {
        document.body.style.height = "100vh";
        document.getElementById("game").style.display = "none";
        document.getElementById("score").style.display = "flex";
        if (winValue === 8) {
          if (localStorage.getItem("easyHighScore")) {
            if (count < parseInt(localStorage.getItem("easyHighScore"))) {
              localStorage.setItem("easyHighScore", count);
            }
          } else {
            localStorage.setItem("easyHighScore", count);
          }
          document.getElementById("score").innerText = `Game Over
      Your Score is ${count}
      High Score: ${localStorage.getItem("easyHighScore")}`;
        } else if (winValue === 16) {
          if (localStorage.getItem("mediumHighScore")) {
            if (count < parseInt(localStorage.getItem("mediumHighScore"))) {
              localStorage.setItem("mediumHighScore", count);
            }
          } else {
            localStorage.setItem("mediumHighScore", count);
          }
          document.getElementById("score").innerText = `Game Over
      Your Score is ${count}
      High Score: ${localStorage.getItem("mediumHighScore")}`;
        } else {
          if (localStorage.getItem("hardHighScore")) {
            if (count < parseInt(localStorage.getItem("hardHighScore"))) {
              localStorage.setItem("hardHighScore", count);
            }
          } else {
            localStorage.setItem("hardHighScore", count);
          }
          document.getElementById("score").innerText = `Game Over
      Your Score is ${count}
      High Score: ${localStorage.getItem("hardHighScore")}`;
        }

        document.getElementById("restart").remove();
        document.getElementById("live-score").remove();
        document.getElementById("menu").style.width = "20rem";
        document.getElementById("menu").style.fontSize = "2rem";
      }, 500);
    }
  }
}

// when the DOM loads
function setUpGame() {
  document.body.style.height = "100%";
  document.getElementById("live-score").style.display = "block";
  document.getElementById("main-menu").style.display = "none";
  document.getElementById("score").style.display = "none";
  document.getElementById("game").style.display = "flex";
  document.getElementById("restart").style.display = "block";
  document.getElementById("menu").style.display = "block";
  let shuffledColors = shuffle();

  if (document.getElementById("select-difficulty").value === "medium") {
    winValue = 16;
  } else if (document.getElementById("select-difficulty").value === "hard") {
    winValue = 24;
  }

  if (winValue === "24") {
    document.getElementById("game").style.width = "75vw";
  }
  createDivsForColors(shuffledColors);
}

function removeParent(event) {
  event.target.parentElement.remove();
}

function confirmRestart() {
  if (document.getElementById("confirm")) {
    return;
  }

  const confirm = document.createElement("div");
  confirm.setAttribute("id", "confirm");
  confirm.innerText = "Are You Sure?";

  const yes = document.createElement("button");
  yes.innerText = "Yes";
  yes.onclick = restart;
  confirm.append(yes);

  const no = document.createElement("button");
  no.innerText = "No";
  no.onclick = removeParent;
  confirm.append(no);

  document.getElementById("restart").insertAdjacentElement("afterend", confirm);
}

function restart(event) {
  document.getElementById("game").innerHTML = "";
  document.getElementById("live-score").innerText = "0";
  removeParent(event);
  previousId = null;
  previousClass = null;
  count = 0;
  openCards = 0;
  isFlipped = 0;
  setUpGame();
}

function mainMenu() {
  window.location.reload();
}

function initialize() {
  winValue = 8;
}

initialize();
